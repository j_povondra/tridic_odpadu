from django.db import models

class Popelnice(models.Model):
    nazev = models.CharField(max_length=250, null=True, blank=False)
    obrazek = models.ImageField(blank=False, upload_to="images/popelnice/")

    def __str__(self):
        return self.nazev

    class Meta:
        verbose_name = 'Popelnice'
        verbose_name_plural = 'Popelnice'

class Predmet(models.Model):
    nazev = models.CharField(max_length=250, null=True, blank=False)
    popis = models.TextField(null=True, blank=True)
    obrazek = models.ImageField(blank=False, upload_to="images/predmety/")
    umisteni = models.ForeignKey(Popelnice, on_delete=models.CASCADE)

    def __str__(self):
        return self.nazev

    class Meta:
        verbose_name = 'Předmět'
        verbose_name_plural = 'Předměty'
