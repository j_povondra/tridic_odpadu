from django.shortcuts import render
from .models import Popelnice, Predmet
from django.http import HttpResponse
from django.core import serializers
from .game_sessions import GameSession, get_instance_by_session

import json
import uuid


def trideni_odpadu(request):
    vsechny_predmety = Predmet.objects.count()

    # Vzdy začít novou session při vstupu na stránku
    game_id = str(uuid.uuid4())
    request.session['game_id'] = game_id


    queryset = Predmet.objects.order_by('?')[:20]
    id_list = list(queryset.values_list('id', flat=True))


    game_session = GameSession(game_id)
    game_session.save_used_ids(id_list)

    context = {
        'predmety': queryset,
        'popelnice': Popelnice.objects.all(),
        'vsechny_predmety' : vsechny_predmety
    }

    return render(request, 'index.html', context)

def ajax(request, id):

    predmet = Predmet.objects.filter(id=id).first()

    context = {
        'nazev': predmet.nazev,
        'popis':  predmet.popis,
    }
    return HttpResponse(json.dumps(context), content_type='application/json')

def load_new_items(request):
    game_session = get_instance_by_session(request.session['game_id'])

    if game_session:
        queryset = Predmet.objects.order_by('?').exclude(id__in=game_session.get_used_ids())[:20]
        qs_json = serializers.serialize('json', queryset)
        return HttpResponse(qs_json, content_type='application/json')
    else:
        return HttpResponse(status=204)