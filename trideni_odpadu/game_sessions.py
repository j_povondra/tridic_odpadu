
class GameSession():
    session_id = None
    used_items = []
    instances = []

    def __init__(self, session: str):
        self.session_id = session
        GameSession.instances.append(self)

    def get_used_ids(self):
        return self.used_items

    def save_used_ids(self, list):
        self.used_items=self.used_items + list


def get_instance_by_session(session):
    for instance in GameSession.instances:
        if instance.session_id == session:
            return instance
    return None