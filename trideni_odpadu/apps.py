from django.apps import AppConfig


class TrideniOdpaduConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'trideni_odpadu'
