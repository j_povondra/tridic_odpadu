from django.contrib import admin

from .models import Popelnice, Predmet

admin.site.register(Popelnice)
admin.site.register(Predmet)