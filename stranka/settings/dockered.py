from .base import *
import dj_database_url

DEBUG = False

DATABASES = {
    "default": dj_database_url.config('DB_URL',
        default=SECRETS.DB_URL,
    )
}
